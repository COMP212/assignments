﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delegate
{
    public partial class Form1 : Form
    {
        private Publisher _publisher;

        public Form1()
        {
            InitializeComponent();
            _publisher = new Publisher();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(_publisher);
            form2.ShowDialog(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(_publisher);
            form3.ShowDialog(this);
        }
    }
}
