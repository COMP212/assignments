﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delegate
{
    public partial class Form3 : Form
    {
        private readonly Publisher _publisher;

        public Form3(Publisher publisher)
        {
            _publisher = publisher;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _publisher.Publish(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
