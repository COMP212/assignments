﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Delegate
{
    public class Publisher
    {
        public delegate void PublishMessageDelegate(string message);

        public PublishMessageDelegate Publish;

        private readonly List<string> _emails = new List<string>();

        private readonly List<string> _phones = new List<string>();

        public Publisher()
        {
            Publish += SendEmails;
            Publish += TextToPhones;
        }

        public void SubscribeEmail(string email)
        {
            if (!_emails.Contains(email, StringComparer.OrdinalIgnoreCase))
            {
                _emails.Add(email);
            }
        }

        public void UnsubscribeEmail(string email)
        {
            var e = _emails.Find(x => x.Equals(email, StringComparison.OrdinalIgnoreCase));
            if (e != null)
            {
                _emails.Remove(e);
            }
        }

        public void SubscribePhone(string phone)
        {
            if (!_phones.Contains(phone, StringComparer.OrdinalIgnoreCase))
            {
                _phones.Add(phone);
            }
        }

        public void UnsubscribePhone(string phone)
        {
            var e = _phones.Find(x => x.Equals(phone, StringComparison.OrdinalIgnoreCase));
            if (e != null)
            {
                _phones.Remove(e);
            }
        }

        private void SendEmails(string message)
        {
            MessageBox.Show("Sending email...\n" + message);
        }

        private void TextToPhones(string message)
        {
            MessageBox.Show("Sending text message...\n" + message);
        }
    }
}
