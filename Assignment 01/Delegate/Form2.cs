﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Delegate
{
    public partial class Form2 : Form
    {
        private Publisher _publisher;

        public Form2(Publisher publisher)
        {
            _publisher = publisher;
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!checkBox1.Checked && !checkBox2.Checked)
            {
                MessageBox.Show("Please Check at least one Check Box");
                return;
            }

            if (checkBox1.Checked)
            {
                bool validation = CheckEmail();
                if (validation == false)
                    return;

                _publisher.SubscribeEmail(textBox1.Text);
            }

            if (checkBox2.Checked)
            {
                bool validation = CheckPhone();
                if (validation == false)
                    return;

                _publisher.SubscribePhone(textBox2.Text);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!checkBox1.Checked && !checkBox2.Checked)
            {
                MessageBox.Show("Please Check at least one Check Box");
                return;
            }

            if (checkBox1.Checked)
            {
                _publisher.UnsubscribeEmail(textBox1.Text);
            }

            if (checkBox2.Checked)
            {
                _publisher.UnsubscribePhone(textBox2.Text);
            }

            checkBox1.Checked = checkBox2.Checked = false;
            textBox1.Text = textBox2.Text = string.Empty;
        }

        private bool CheckEmail()
        {
            Regex validationOfEmail = new Regex(@"^[a-z][\w\.-]*@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$", RegexOptions.IgnoreCase);
            bool validation = validationOfEmail.IsMatch(textBox1.Text);
            if (validation == false)
            {
                MessageBox.Show("This is an invalid email address. Please enter a valid email address.");
            }
            return validation;
        }

        private bool CheckPhone()
        {
            Regex validationOfPhoneNumber = new Regex(@"^([0-9]{3}[\-][0-9]{3}[\-][0-9]{4})$");
            bool validation = validationOfPhoneNumber.IsMatch(textBox2.Text);
            if (validation == false)
            {
                MessageBox.Show("This is an invalid phone number. Please enter a valid phone number.");
            }
            return validation;
        }
    }
}
